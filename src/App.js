import './App.css';
import ScreenSettings from './components/ScreenSettings';
import ScreenCrossroads from './components/ScreenCrossroads'


import React, { useState } from 'react';



function App() {
  const [screen, setSreen] = useState('setting')
  const [mode, setMode] = useState()

  return (
    <>
      {screen === 'setting' && <ScreenSettings changeScreen={setSreen} setMode={setMode}/>}
      {screen === 'crossroad' && <ScreenCrossroads changeScreen={setSreen} mode={mode}/>}    
    </>
  );
}

export default App;

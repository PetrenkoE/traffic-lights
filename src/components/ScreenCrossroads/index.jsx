import React, { useEffect, useState } from 'react';
import Light from '../Lights';
import './style.css';

const CrossRoad = (props) => {
  const [lightState, setLightState] = useState([]);
  
  useEffect(() => {
    switch (props.mode.mode) {
      case 'serial':
        setLightState(['active', 'inactive', 'inactive', 'inactive']);
        break;
      case 'opposite':
        setLightState(['inactive', 'active', 'inactive', 'active']);
        break;
      case 'right':
        setLightState(['active', 'arrow-active', 'active', 'arrow-active']);
        break;
      default:
        setLightState(['active', 'inactive', 'inactive', 'inactive']);
    }
  }, []);

  useEffect(() => {
    const changePhaseInterval = setInterval(
      changePhase,
      +props.mode.duration * 1000
    );
    return () => clearInterval(changePhaseInterval);
  });

  const changePhase = () => {
    switch (props.mode.mode) {
      case 'serial':
        serialChange();
        break;
      case 'opposite':
        oppositeChage();
        break;
      case 'right':
        rightChange();
        break;
      default:
    }
  };

  const serialChange = () => {
    let crossRoadState = [...lightState];
    let currentActive = crossRoadState.findIndex(
      (status) => status === 'active'
    );
    crossRoadState[currentActive] = 'inactive';
    if (currentActive === crossRoadState.length - 1) {
      currentActive = -1;
    }
    crossRoadState[currentActive + 1] = 'active';
    setLightState(crossRoadState);
  };

  const oppositeChage = () => {
    let crossRoadState = [...lightState];
    const currentActive = crossRoadState.findIndex(
      (status) => status === 'active'
    );
    if (currentActive === 0 || currentActive === 2) {
      crossRoadState = ['inactive', 'active', 'inactive', 'active'];
    } else {
      crossRoadState = ['active', 'inactive', 'active', 'inactive'];
    }
    setLightState(crossRoadState);
  };

  const rightChange = () => {
    let crossRoadState = [...lightState];
    const currentActive = crossRoadState.findIndex(
      (status) => status === 'active'
    );
    if (currentActive === 0 || currentActive === 2) {
      crossRoadState = ['arrow-active', 'active', 'arrow-active', 'active'];
    } else {
      crossRoadState = ['active', 'arrow-active', 'active', 'arrow-active'];
    }
    setLightState(crossRoadState);
  };
  if (lightState.length === 0) return null;
  return (
    <>
      <button onClick={() => props.changeScreen('setting')}>настройки</button>
      <div className='wrapper'>
        <div style={{ position: 'relative' }}>
          <Light
            style={{ transform: 'rotate(180deg)', bottom: 0, right: 0 }}
            currentStatus={lightState[0]}
          />
        </div>
        <div className='lane-vertical'>
          <div></div>
        </div>
        <div className='lane-vertical'>
          <div></div>
        </div>
        <div style={{ position: 'relative' }}>
          <Light
            style={{ transform: 'rotate(270deg)', bottom: -15, left: 15 }}
            currentStatus={lightState[1]}
          />
        </div>
        <div className='lane-horizontal'>
          <div></div>
          <div></div>
        </div>
        <div className='inner-square'></div>
        <div className='inner-square'></div>
        <div className='lane-horizontal'>
          <div></div>
          <div></div>
        </div>
        <div className='lane-horizontal'>
          <div></div>
          <div></div>
        </div>
        <div className='inner-square'></div>
        <div className='inner-square'></div>
        <div className='lane-horizontal'>
          <div></div>
          <div></div>
        </div>
        <div style={{ position: 'relative' }}>
          <Light
            style={{ transform: 'rotate(90deg)', top: -15, right: 15 }}
            currentStatus={lightState[3]}
          />
        </div>
        <div className='lane-vertical'>
          <div></div>
        </div>
        <div className='lane-vertical'>
          <div></div>
        </div>
        <div style={{ position: 'relative' }}>
          <Light style={{ top: 0, left: 0 }} currentStatus={lightState[2]} />
        </div>
      </div>
    </>
  );
};

export default CrossRoad;

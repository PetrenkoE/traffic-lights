import React from 'react';
import "./style.css";

 const Light =(props)=>{

return (
    <div className={`light ${props.currentStatus}`} style ={props.style}>
        <div className={`red`}><div></div></div>
        <div className={`yellow`}><div></div></div>
        <div className={`green`}><div></div></div>
        <div className={`arrow`}><div></div></div>
    </div>
)

}
export default Light;
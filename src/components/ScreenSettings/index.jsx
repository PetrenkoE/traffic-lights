import React, { useState } from 'react';
import { Container, Button, Row, Col} from 'react-bootstrap';
import './style.css';


const ScreenSettings = (props) => {
  const [mode, setMode] = useState('serial');
  const [duration, setDuration] = useState();
  const modeChangeHandler = (event) => {
    switch (event.target.value) {
      case 'serial':
        setMode('serial');
        break;
      case 'opposite':
        setMode('opposite');
        break;
      case 'right':
        setMode('right');
        break;
      default:
        setMode('serial');
    }
  };

  const saveButtonClickHandler = () => {
    props.setMode({
      mode,
      duration,
    });
    props.changeScreen('crossroad');
  };

  return (
    <>
    
      <Container overflow-hidden>
      
        <Row>
          <Col>
                      <h1 className='header'>Настройте работу светофора на перекрестке</h1>
            <p className='Explanation'>
              Для настройки работы светофора выберите режим и укажите
              продолжительность фазы в секундах
            </p>
            
          </Col>
        </Row>
        <Col>
          <p className='mode'>Режимы работы светофора:</p>
        </Col>
        <Col>
          <label>
            <input
              type='radio'
              name='mode'
              value='serial'
              checked={mode === 'serial'}
              onChange={modeChangeHandler}
            />{' '}
            Последовательный
          </label>
        </Col>
        <br />
        <Col>
          <label>
            <input
              type='radio'
              name='mode'
              value='opposite'
              checked={mode === 'opposite'}
              onChange={modeChangeHandler}
            />{' '}
            Противоположный
          </label>
        </Col>
        <br />
        <Col>
          <label>
            <input
              type='radio'
              name='mode'
              value='right'
              checked={mode === 'right'}
              onChange={modeChangeHandler}
            />{' '}
            C поворотом направо
          </label>
        </Col>
        <br />
        <Col>
        
          <input
            type='number'
            placeholder='Введите секунды'
            id='seconds'
            min='0'
            step='1'
            value={duration}
            onInput={(e) => {
              setDuration(e.target.value);
            }}
          />
          
        </Col>
        <br />
        <Col>
        <Button onClick={saveButtonClickHandler} type='button'  id='save'>Сохранить</Button>     
           
           
                </Col>
                
      </Container>
      
    </>
  );
};

export default ScreenSettings;
